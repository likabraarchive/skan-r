<?php

if ( ! function_exists('custom_post_type') ) {

// Register Custom Post Type
function custom_post_type() {

	$labels = array(
		'name'                => _x( 'Produkter', 'Post Type General Name', 'likabra' ),
		'singular_name'       => _x( 'Produkt', 'Post Type Singular Name', 'likabra' ),
		'menu_name'           => __( 'Produkter', 'likabra' ),
		'parent_item_colon'   => __( 'Förälder:', 'likabra' ),
		'all_items'           => __( 'Alla produkter', 'likabra' ),
		'view_item'           => __( 'Visa produkt', 'likabra' ),
		'add_new_item'        => __( 'Ny produkt', 'likabra' ),
		'add_new'             => __( 'Skapa ny', 'likabra' ),
		'edit_item'           => __( 'Redigera produkt', 'likabra' ),
		'update_item'         => __( 'Uppdatera produkt', 'likabra' ),
		'search_items'        => __( 'Sök produkt', 'likabra' ),
		'not_found'           => __( 'Hittades ej', 'likabra' ),
		'not_found_in_trash'  => __( 'Hittades ej i papperskorgen', 'likabra' ),
	);
	$args = array(
		'label'               => __( 'products', 'likabra' ),
		'description'         => __( 'Företagets produkter', 'likabra' ),
		'labels'              => $labels,
		'supports'            => array( 'title', 'editor', 'excerpt', 'author', 'thumbnail', 'comments', 'trackbacks', 'revisions', 'custom-fields', 'page-attributes', 'post-formats', ),
		'taxonomies'          => array( 'category', 'post_tag' ),
		'hierarchical'        => false,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => true,
		'show_in_admin_bar'   => true,
		'menu_position'       => 5,
		'menu_icon'           => 'dashicons-portfolio',
		'can_export'          => true,
		'has_archive'         => true,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'capability_type'     => 'post',
	);
	register_post_type( 'products', $args );

}

// Hook into the 'init' action
add_action( 'init', 'custom_post_type', 0 );

}