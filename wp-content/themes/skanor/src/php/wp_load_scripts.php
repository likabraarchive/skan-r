<?php





// Register scripts for specific template only
if ( ! function_exists('custom_scripts_and_styles_for_startsida') ) {

// Register Scripts
function custom_scripts_startsida() {

	if ( is_page_template('templates/start.php') ) {
		wp_register_script( 'owl', get_template_directory_uri() . '/src/frameworks/owl/owl.carousel.js', array( 'bootstrap' ), false, true ); // load in footer
		wp_enqueue_script( 'owl' );

		wp_register_script( 'owlsettings', get_stylesheet_directory_uri() . '/src/js/owlsettings.js', array( 'owl' ), false, true ); // load in footer
		wp_enqueue_script( 'owlsettings' );
	}

}
// Hook into the 'wp_enqueue_scripts' action if page template is Startsida
add_action( 'wp_enqueue_scripts', 'custom_scripts_startsida' );



// Register Styles
function custom_styles_startsida() {

	if ( is_page_template('templates/start.php') ) {
		wp_register_style( 'owl', get_template_directory_uri() . '/src/frameworks/owl/owl.carousel.css', array('bootstrap'), false );
		wp_enqueue_style( 'owl' );
		
		wp_register_style( 'owltheme', get_template_directory_uri() . '/src/frameworks/owl/owl.theme.css', array('owl'), false );
		wp_enqueue_style( 'owltheme' );
	}
	
}
// Hook into the 'wp_enqueue_scripts' action if page template is Startsida
add_action( 'wp_enqueue_scripts', 'custom_styles_startsida' );



}






if ( ! function_exists('custom_scripts_and_styles') ) {

// Register Scripts
function custom_scripts() {

	wp_register_script( 'bootstrap', get_template_directory_uri() . '/src/frameworks/bootstrap/js/bootstrap.min.js', array( 'jquery' ), false, true ); // load in footer
	wp_enqueue_script( 'bootstrap' );

	wp_register_script( 'global', get_template_directory_uri() . '/src/js/global.js', array( 'bootstrap' ), false, true ); // load in footer
	wp_enqueue_script( 'global' );

}
// Hook into the 'wp_enqueue_scripts' action
add_action( 'wp_enqueue_scripts', 'custom_scripts' );





// Ugly Custom scripts for IE (wp_enqueue_scripts can't handle If IE)
add_action( 'wp_head', function() {
	echo '<!--[if lt IE 9]>';
    echo '<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>';
    echo '<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>';
    echo '<![endif]-->';
} );





// Register Styles
function custom_styles() {

	wp_register_style( 'bootstrap', get_template_directory_uri() . '/src/frameworks/bootstrap/css/bootstrap.min.css', array(), false );
	wp_enqueue_style( 'bootstrap' );

	wp_register_style( 'gravityforms', get_template_directory_uri() . '/src/css/gravityforms.css', array('bootstrap'), false );
	wp_enqueue_style( 'gravityforms' );

	wp_register_style( 'likabra', get_template_directory_uri() . '/src/css/likabra.min.css', array('gravityforms'), false );
	wp_enqueue_style( 'likabra' );

	wp_register_style( 'default', get_stylesheet_directory_uri() . '/src/css/child.css', array('gravityforms'), false );
	wp_enqueue_style( 'default' );
}
// Hook into the 'wp_enqueue_scripts' action
add_action( 'wp_enqueue_scripts', 'custom_styles' );



}





