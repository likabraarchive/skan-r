<?php get_header(); ?>

      

<section id="page">
<!-- Start page -->
<div class="container">
	<div class="row">
	
		<div id="main" class="col-sm-9">


			<header class="page-header">

				<?php if(is_category()) :
					echo '<h1>Nyheter <small>' . single_cat_title('', false) . '</small></h1>';
				elseif(is_tag()) :
					echo '<h1>Nyheter <small>' . single_tag_title('', false) . '</small></h1>';
				elseif(is_post_type_archive()) :
					echo '<h1>' . post_type_archive_title('', false) . '</h1>';
				elseif(is_archive()) :
					echo '<h1>Nyheter <small>' . single_month_title(' ', false) . '</small></h1>';
				elseif(is_search()) :
					echo '<h1>Sökresultat <small>' . get_search_query() . '</small></h1>';
				else :
					echo '<h1>Nyheter</h1>';
				endif;
    			?>

			</header>



<section class="list-view">

<!-- Start the loop -->
<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>


	<!-- Use template -->
	<?php include("src/views/list.php"); ?>
	
	

<!-- End the loop (include pagination)-->
<?php		
		endwhile;
			bootstrap_pagination();
		else :
		// If no content...
		echo '<article>Oops! Här fanns visst inget...</article>';
	endif;
?>

</section><!-- .list-view -->



<hr />

		</div><!-- #main -->


<?php get_sidebar(); ?>

		
	</div><!-- .row -->
</div><!-- end .container -->
<!-- End page -->
</section>


      
<?php get_footer(); ?>      




