<!-- Grid-item template -->

<?php
$title = get_the_title();
$copy = get_the_excerpt( );
$link = get_permalink();
$image = '<img class="gridimage" src="' . get_stylesheet_directory_uri() . '/src/images/default.png"/>';

if (has_post_thumbnail()){
	$image = get_the_post_thumbnail(null, 'Pagewidth', array('class' => 'gridimage'));
}

$str = <<<EOF



<article class="col-sm-4 grid-item">
	<a class="hidden-xs" href="$link">
		$image
	</a>
	<div class="grid-copy">
		<h4><a href="$link" >$title</a></h4>
		<p>$copy</p>
	</div>
</article><!-- .grid-item -->



EOF;
    echo $str;


?>
