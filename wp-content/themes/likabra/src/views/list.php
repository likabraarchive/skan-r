<!-- List-item template -->

<?php
$title = get_the_title();
$copy = get_the_excerpt( );
$link = get_permalink();
$image = '<img class="listimage" src="' . get_stylesheet_directory_uri() . '/src/images/default.png"/>';

if (has_post_thumbnail()){
	$image = get_the_post_thumbnail(null, 'Pagewidth', array('class' => 'listimage'));
}

$str = <<<EOF



<article class="list-item">			
	<div class="row">
    	<a class="hidden-xs col-sm-3" href="$link">
			$image
		</a>
    	<div class="col-sm-9">
      		<h4><a href="$link" >$title</a></h4>
	  		<p>$copy</p>
	  	</div>
	</div>
</article><!-- .list-item -->



EOF;
    echo $str;


?>
