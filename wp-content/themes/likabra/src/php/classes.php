<?php





function displayWithLabel($label, $text ,  $after ="" )
{

	if ( strlen($text)>1  )
	{
		return $label . " " . $text . $after;

	}


}



class universalRepeater {
 	
  public function __construct($repeater= "name",  $template="list-item.php"     ){
    
	
	$this->repeater = $repeater;
	$this->template = $template;
	$this->path =  get_stylesheet_directory();



  }
 
  public function display(){
  
 if (have_rows($this->repeater)) {
 	while ( have_rows($this->repeater) ) 
 	{
 	 the_row();
 		include($this->path."/src/views/".$this->template);
 	}
 	}
} //end function
 
	public function displayOptions($option){
		if (have_rows($this->repeater, $option) ) {
			while ( have_rows($this->repeater, $option) ) 
			{
			the_row();
			include($this->path."/src/views/".$this->template);
			}
		}
 	
	} //end function
   
}//end cl







  
  
  
  
  
  
  class acf_relation_repeater {
 	//used when using relationfield
 	 	
  public function __construct($field,  $template="list-item.php"  , $option=false   ){
    
	
	$this->posts =  get_field($field, $option);
	$this->template = $template;
	$this->path = get_stylesheet_directory();
	$this->usedID = array();
		

	
}
 
 
  public function displayAlternative ($smallTemplate, $bigTemplate, $count )
 	{
	 	  global $post;
	 	  $current = 0;
	 	  
  	if( $this->posts) {
	  	foreach( $this->posts as $post)
	  		{
	  		
	  		 
	  		
	  		setup_postdata($post);
	  		 if ($current  < $count  )
	  		 	{
		  		include($this->path."/src/views/".$bigTemplate);
		  		}
		  	else
		  		{
			  		
			  		include($this->path."/src/views/".$smallTemplate);
		  		}	  	
		  	$current ++;
	  	
  		}
  	wp_reset_postdata();
  	} // end display
  	
  	

}


 public function displayCustom ($bigTemplate, $smallTemplate, $displayBig )
 	{
	 	  global $post;
	 	  $current = 1;
  	if( $this->posts) {
	  	foreach( $this->posts as $post)
	  		{
	  		setup_postdata($post);
	  		 if ( in_array($current, $displayBig )    )
	  		 	{
		  		include($this->path."/src/views/".$bigTemplate);
		  		}
		  	else
		  		{
			  		
			  		include($this->path."/src/views/".$smallTemplate);
		  		}	  	
		  	$current ++;
	  	
  		}
  	wp_reset_postdata();
  	} // end display
  	
  	

}

	 	
 	public function getUsedId()
 	{
 		return $this->usedID;
 	}

 
 
 
  public function display(){
  global $post;
  
  	if( $this->posts) {
	  	foreach( $this->posts as $post)
	  		{
	  		setup_postdata($post);
	  		//used to exclude posts in latter lists
	  		array_push ($this->usedID, get_the_id() );
		  		include($this->path."/src/views/".$this->template);
		  			  	
	  	
  		}
  	wp_reset_postdata();
  	} // end display
  	
  	

}
  	 public function countPosts ()
 	{
 		if(is_array($this->posts))
 		{
	 		$num = count ( $this->posts  );
	 	}
	 	else
	 	{$num = 0;}	
	 	return $num;
	 	
 	}
}//end class






class wp_queryTemplate {
 	
 	 	
  public function __construct($args,  $template="list-item.php"     ){
    
	
	$this->args = $args;
	$this->template = $template;
	$this->path = get_stylesheet_directory();

	$this->query =  new WP_Query( $args );

}
 
  public function display(){
  	if ($this->query->have_posts() ) {
    	 		
    	 
				while ( $this->query->have_posts() ) {
					
					$this->query->the_post();
					include($this->path."/src/views/".$this->template);
				
					}
		} 
	else {
		include($path."/src/views/empty.php");	// no posts found
		}
	wp_reset_postdata();
 
 
  } //end function
 
  
}


class LikaBraGallery
{
  private $thumbnails;
  private $bigimages;
  private $imageArray;
  public $thumbMarkup;


  public function __construct($field="galleri",$subField=false)
    {
    	$this->path = get_stylesheet_directory();


    	//added support for subfields

      if (function_exists("get_field")  && !$subField  )
      {
        $this->imageArray = get_field($field);
      }
      if (function_exists("get_field")  && $subField  )
      {
      	 $this->imageArray = get_sub_field($field);

      }
     
     //preload images...

    }



    public function displayThumbs($template)
    {

    	$this->thumbMarkup ="";

    	 if ($this->imageArray)
      {
        foreach(  $this->imageArray as $image ) 
        {

        	
           // echo $this->path."/src/views/".$this->template ;

           include($this->path."/src/views/".$template);

        }

      }


     return $this->thumbMarkup;
     
    }
    
  public function displayImages()
  {
     if ($this->imageArray)
      {
        foreach(  $this->imageArray as $image ) 
        {


            $img=  $image["bild"]["sizes"]['Fullwidth'];
            

            $this->bigImages.= "
                                <div>
                                <img src='$img' alt='' />
                                </div>";


        }

      }
      return $this->bigImages;


  }



} // end class



