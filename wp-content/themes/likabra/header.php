<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="<?php bloginfo('stylesheet_directory'); ?>/src/images/ico/favicon.ico">

    <title><?php bloginfo('name'); ?><?php wp_title(); ?></title>
    
    <?php wp_head(); ?>
    
  </head>

  <body <?php body_class(); ?>>


	

      <!-- Fixed navbar -->
	  <nav class="navbar navbar-default navbar-fixed-top" role="navigation">
	      <!-- Brand and toggle get grouped for better mobile display -->
	      <div class="container">
	          <div class="navbar-header">
	              <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
	                  <span class="sr-only">Toggle navigation</span>
	                  <span class="icon-bar"></span>
	                  <span class="icon-bar"></span>
	                  <span class="icon-bar"></span>
	              </button>
	  
	              <a class="navbar-brand" href="<?php echo home_url(); ?>">
	                  <?php bloginfo('name'); ?>
	              </a>
	          </div>
	  
	          <?php
	              wp_nav_menu( array(
	                  'theme_location'    => 'Primary',
	                  'depth'             => 2,
	                  'container'         => 'div',
	                  'container_class'   => 'collapse navbar-collapse navbar-ex1-collapse',
	                  'menu_class'        => 'nav navbar-nav navbar-right',
	                  'fallback_cb'       => 'wp_bootstrap_navwalker::fallback',
	                  'walker'            => new wp_bootstrap_navwalker())
	              );
	          ?>
	      </div>
	  </nav>      

