<div id="sidebar" class="col-sm-3">
	
<?php if ( is_active_sidebar( 'right-sidebar' ) ) : ?>					

    	<ul class="widgets" ><?php dynamic_sidebar( 'right-sidebar' ); ?></ul>

<?php else : ?>

    <!-- This content shows up if there are no widgets defined in the backend. -->

<?php endif; ?>
	
</div><!-- End #sidebar -->


