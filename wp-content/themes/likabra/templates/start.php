<?php 
/**
 * Template Name: Startsida
 */

get_header(); ?>



<section id="page">
<!-- Begin page content -->
      <div class="container">
      	<div class="row">
      		<div id="main" class="col-sm-12">


<!-- Start the loop -->
<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

					


		<?php the_title( '<header class="page-header"><h1>', '</h1></header>' ); ?>

<!-- Start slideshow -->
<article class="slideshow">

<?php
$images = get_field('galleri');
 

/* If we have images in the gallery... */
if( $images ): ?>



<div id="start-top" class="owl-carousel">

  <?php foreach( $images as $image ): ?>

    <div>
      <img src="<?php echo $image['sizes']['Fullwidth']; ?>" alt="<?php echo $image['alt']; ?>" />
    </div>
    
  <?php endforeach; 
	    endif;
  ?>  
    
</div><!-- .owl-carousel -->


<hr />

</article>
<!-- End Slideshow -->


<!-- Start Content -->
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<div class="page-content">
		<?php the_content(); ?>
	</div><!-- .page-content -->
	<hr />	

</article><!-- #post-## -->
	
	
					
		<!-- End the loop -->
		<?php	
				endwhile;
				// Previous/next post navigation.
			else :
				// If no content...
				echo '<article>Oops! Något gick snett...</article>';
			endif;
		?>
		
		
<!-- End Content -->
		


<!-- Start Puffar -->
<section class="row puffar" >


  <article class="col-sm-6 puff" >

<?php 
 
$image = get_field('vbild');
 
if( !empty($image) ): ?>
	<img src="<?php echo $image['sizes'][ 'Pagewidth' ]; ?>" alt="<?php echo $image['alt']; ?>" />
<?php endif; ?>

    <h3><?php the_field('vrubrik'); ?></h3>
    <?php the_field('vtext'); ?>
    <p><a href="<?php the_field('vlank'); ?>" class="btn btn-primary" role="button">Läs mer</a>
  </article>
  





  <article class="col-sm-6" >

<?php 
 
$image = get_field('hbild');
 
if( !empty($image) ): ?>
	<img src="<?php echo $image['sizes'][ 'Pagewidth' ]; ?>" alt="<?php echo $image['alt']; ?>" />
<?php endif; ?>

    <h3><?php the_field('hrubrik'); ?></h3>
    <?php the_field('htext'); ?>
    <p><a href="<?php the_field('hlank'); ?>" class="btn btn-primary" role="button">Läs mer</a>

  </article>


</section><!-- end .puffar -->
<!-- Slut Puffar -->		
      		
      		
      		</div><!-- end .main -->

      	</div><!-- end .row -->
      </div><!-- end .container -->
<!-- End page content -->
</section><!-- end #page -->

      
<?php get_footer(); ?>      



